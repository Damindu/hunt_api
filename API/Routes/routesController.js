module.exports = function(app) {
  var apiControl = require('../Controller/apiController');

  // todoList Routes
  app.route('/tasks')
    .get(apiControl.list_all_tasks)
    .post(apiControl.create_a_task);


  app.route('/tasks/:taskId')
    .get(apiControl.read_a_task)
    .put(apiControl.update_a_task)
    .delete(apiControl.delete_a_task);
};